<?php

namespace App;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserModel extends Authenticatable implements JWTSubject
{
    protected $table = "users";
    protected $primaryKey = 'user_id';

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function login(Request $request)
    {
        $where = [
            'email' => $request->input('email'),
            'password' => md5($request->input('password'))
        ];
        $user = UserModel::where($where)->first();

        if ($user) {
            $resp = new DataResp;
            $userToken = JWTAuth::fromUser($user);
            return $resp->getResponse($user, Response::HTTP_OK, null, false, $userToken);
        } else {
            return null;
        }
    }

}
