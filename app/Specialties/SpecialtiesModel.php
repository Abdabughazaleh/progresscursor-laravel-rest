<?php

namespace App\Specialties;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SpecialtiesModel extends Model
{
    protected $table = "specializations";


    public function getAll()
    {
        return DB::table($this->table)
            ->orderBy('spec_name')
            ->get();
    }

    public function getSpecById($specId)
    {
        return DB::table($this->table)
            ->where('main_spec', '=', $specId)
            ->first();
    }


}
