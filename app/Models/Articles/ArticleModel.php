<?php

namespace App\Models\Articles;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class ArticleModel extends Authenticatable
{
    protected $primaryKey = "art_id";
    protected $table = "articles";
    private $resp;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->resp = new DataResp;

    }

    public function getArticlesByCatId($catId)
    {
        $arts = DB::table($this->table)
            ->where('art_status', 1)
            ->where($this->table . '.cat_id', '=', $catId)
            ->leftJoin('doctors as d1', $this->table . '.doctor_id', '=', 'd1.doctor_id')
            ->leftJoin('articles_cats as ac', $this->table . '.cat_id', '=', 'ac.cat_id')
            ->orderBy('art_id', 'desc')->get();
        return $arts;
    }

    public function getCategories()
    {
        $cats = DB::table('articles_cats')
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from($this->table)
                    ->whereRaw($this->table . '.cat_id = articles_cats.cat_id');
            })->orderBy('order_no', 'desc');
        return $cats->get();
    }

    public function getAllArticles(Request $request)
    {
        $arts = DB::table($this->table)
            ->where('art_status', 1)
            ->leftJoin('doctors as d1', $this->table . '.doctor_id', '=', 'd1.doctor_id')
            ->leftJoin('articles_cats as ac', $this->table . '.cat_id', '=', 'ac.cat_id')
            ->orderBy('art_id', 'desc')->get();
        return $this->resp->getResponse($arts, Response::HTTP_OK, null, true, null);

    }

    public function getArticleById($artId = null)
    {
        $art = DB::table($this->table)
            ->join('doctors', 'doctors.doctor_id', '=', $this->table . '.doctor_id')
            ->join('articles_cats', 'articles_cats.cat_id', '=', $this->table . '.cat_id')
            ->where('art_id', $artId)
            ->first();

        return $art;
    }


    public function getArticleByDoctorId($doctorId)
    {
        $art = DB::table($this->table)
            ->join('doctors', 'doctors.doctor_id', '=', $this->table . '.doctor_id')
            ->join('articles_cats', 'articles_cats.cat_id', '=', $this->table . '.cat_id')
            ->where($this->table . '.doctor_id', '=', '' . $doctorId . '')
            ->get();

        return $art;
    }


}
