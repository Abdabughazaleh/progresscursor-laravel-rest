<?php

namespace App\Models\Countries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CountriesModel extends Model
{
    protected $table = 'countries';

    public function getAllCountries()
    {
        return DB::table('countries')->orderBy('country_name_ar')->get();
    }

    public function getAllCitiesByCountryId($countryId)
    {
        return DB::table('cities')
            ->where('country_id', $countryId)
            ->orderBy('city_name_ar')->get();
    }
    public function getSubCitiesByCityId($cityId)
    {
        return DB::table('sub_cities')
            ->where('city_id', $cityId)
            ->orderBy('sub_city_name')->get();
    }
}
