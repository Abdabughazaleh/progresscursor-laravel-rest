<?php

namespace App\Models\Doctors;

use App\Helpers\DataResponseStructure as DataResp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class DoctorModel extends Model
{
    use Notifiable;
    protected $primaryKey = "doctor_id";
    protected $table = "doctors";
    private $resp;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->resp = new DataResp;
    }

    public function getDoctorById($doctorId)
    {
        return DB::table($this->table)
            ->where('doctor_id', $doctorId)
            ->leftJoin('specializations', 'specializations.main_spec', '=', $this->table . '.specialty_id')
            ->first();
    }

    public function doctorSearch(Request $request)
    {
        $query = DB::table($this->table);
        if ($request->query('doctorName') != null)
            $query->where('first_name', 'like', $request->query('doctorName') . '%');
        if ($request->query('spec') != null)
            $query->where('specialty_id', '=', $request->query('spec') . '%');
        if ($request->query('subCity') != null)
            $query->where('sub_city', '=', $request->query('subCity'));
        if ($request->query('country') != null)
            $query->where('country_id', '=', $request->query('country'));
        if ($request->query('insuranceCompanies') != null)
            $query->whereIn('doctor_id', DB::table('doctor_insurance_companies')->select('doctor_id')->get());

        $query->where('doctor_status', '=', 1);
        $query->leftJoin('specializations', 'specializations.main_spec', '=', $this->table . '.specialty_id');
        return $query->get();
    }

    public function getArticleByDoctorId($doctorId)
    {
        return DB::table('work_days')
            ->where('doctor_id', $doctorId)
            ->get();
    }

    protected $hidden = [
        'password',
    ];
}
