<?php

namespace App\Helpers;

use App\Helpers\Response as Response;

class DataResponseStructure
{

    private $respHeader;
    private $respData;
    private $response;


    public function setHeader($httpStatus, $message)
    {
        if ($message == null)
            $message = Response::$statusTexts[$httpStatus];
        $this->respHeader = ['httpStatus' => $httpStatus, 'httpMessage' => $message];
    }

    public function setObject($data, $isArray = false, $token = null)
    {
        $this->respData = $data;
        if ($token != null) {
            $this->respData['token'] ='Bearer '. $token;
        }
        return $this->respData;

    }

    public function getResponse($data, $httpStatus, $message = null, $isArray = false, $token = null)
    {
        $objName = !$isArray ? 'object' : 'list';
        self::setHeader($httpStatus, $message);
        self::setObject($data, $isArray, $token);
        return $this->response = ['header' => $this->respHeader, $objName => $this->respData];
    }


}
