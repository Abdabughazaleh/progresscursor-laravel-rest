<?php

namespace App\Http\Controllers\Specialties;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;
use App\Http\Controllers\Controller;
use App\Specialties\SpecialtiesModel;
use Illuminate\Http\Request;

class SpecialtiesController extends Controller
{
    private $specModel;
    private $resp;

    public function __construct()
    {
        $this->resp = new DataResp;
        $this->specModel = new SpecialtiesModel();
    }

    public function getAll()
    {
        $spec = $this->specModel->getAll();
        $spec = $this->resp->getResponse($spec, Response::HTTP_OK, null, true, null);
        return response()->json($spec);
    }

    public function getSpecById(Request $request)
    {
        $specId = $request->route('id');
        $spec = $this->specModel->getSpecById($specId);
        $spec = $this->resp->getResponse($spec, Response::HTTP_OK, null, false, null);
        return response()->json($spec);
    }
}
