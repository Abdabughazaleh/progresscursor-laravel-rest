<?php

namespace App\Http\Controllers\Doctor;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;
use App\Http\Controllers\Controller;
use App\Models\Articles\ArticleModel;
use App\Models\Doctors\DoctorModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DoctorController extends Controller
{
    private $resp;
    private $doctorModel;

    public function __construct()
    {
        $this->resp = new DataResp;
        $this->doctorModel = new DoctorModel();
    }

    public function getDoctorById(Request $request)
    {

        $doctor = $this->doctorModel->getDoctorById($request->route('id'));
        $doctor = $this->resp->getResponse($doctor, Response::HTTP_OK, null, false, null);
        return response()->json($doctor);
    }

    public function doctorSearch(Request $request)
    {
        $doctors = $this->doctorModel->doctorSearch($request);
        $doctors = $this->resp->getResponse($doctors, Response::HTTP_OK, null, true, null);
        return response()->json($doctors);
    }

    public function getDoctorWorkingDays(Request $request)
    {
        $doctorId = $request->route('id');
        $workingDays = $this->doctorModel->getArticleByDoctorId($doctorId);
        $workingDays = $this->resp->getResponse($workingDays, Response::HTTP_OK, null, true, null);
        return response()->json($workingDays);
    }


}
