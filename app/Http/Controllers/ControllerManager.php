<?php

namespace App\Http\Controllers;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;

class ControllerManager extends Controller
{
    public $resp;

    public function __construct()
    {
        $this->resp = new DataResp;
    }

    public function getData($data, $isArray = false, $httpStatus = Response::HTTP_OK, $message = null, $token = null)
    {
        $data = $this->resp->getResponse($data, $httpStatus, $message, $isArray, $token);
        return response()->json($data);
    }


}
