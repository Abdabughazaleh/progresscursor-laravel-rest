<?php

namespace App\Http\Controllers;

use App\UserModel as userModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{


    public function login(Request $request)
    {
        $userData = UserModel::login($request);
        if ($userData['header']['httpStatus'] == '200') {
            return response()->json($userData);
        } else {
            return response()->json(['status' => 'error']);
        }
    }

    public function generateKey(Request $request)
    {


        // return $apy;
        // $user = JWTAuth::parseToken()->authenticate();
        // return $user;
        // return  $user=auth('userAuth')->userOrFail();

    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth('userAuth')->factory()->getTTL() * 1000
        ]);
    }
}
