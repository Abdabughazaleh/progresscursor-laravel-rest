<?php

namespace App\Http\Controllers\Countries;

use App\Helpers\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerManager;
use App\Models\Countries\CountriesModel;
use Illuminate\Http\Request;

class CountriesController extends ControllerManager
{
    private $countriesModel;

    public function __construct()
    {
        parent::__construct();
        $this->countriesModel = new CountriesModel();
    }


    public function getAllCountries(Request $request)
    {
        $countries = $this->countriesModel->getAllCountries();
        return $this->getData($countries, true);
    }

    public function getAllCitiesByCountryId(Request $request)
    {
        $countryId = $request->route('id');
        $cities = $this->countriesModel->getAllCitiesByCountryId($countryId);
        return $this->getData($cities, true);
    }
    public function getSubCitiesByCityId(Request $request)
    {
        $cityId = $request->route('id');
        $cities = $this->countriesModel->getSubCitiesByCityId($cityId);
        return $this->getData($cities, true);
    }
}
