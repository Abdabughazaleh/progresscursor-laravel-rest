<?php

namespace App\Http\Controllers\Articles;

use App\Helpers\DataResponseStructure as DataResp;
use App\Helpers\Response;
use App\Http\Controllers\Controller;
use App\Models\Articles\ArticleModel;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $resp;
    private $articleModel;

    public function __construct()
    {
        $this->resp = new DataResp;
        $this->articleModel = new ArticleModel();
    }

    public function getArticles(Request $request)
    {
        $articles = $this->articleModel->getAllArticles($request);
        return response()->json($articles);
    }

    public function getArticleById(Request $request)
    {
        $art = $this->articleModel->getArticleById($request->route('id'));
        if ($art != null)
            $art = $this->resp->getResponse($art, Response::HTTP_OK, null, false, null);
        else
            $art = $this->resp->getResponse(null, Response::HTTP_NOT_FOUND, null, false, null);

        return response()->json($art);

    }

    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[$i] = self::convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
    }

    public function getArticlesByOrderByCats()
    {

        $cats = $this->articleModel->getCategories();

        foreach ($cats as $cat):
            $cat->articles = $this->articleModel->getArticlesByCatId($cat->cat_id);
        endforeach;
        $cats = $this->resp->getResponse($cats, Response::HTTP_NOT_FOUND, null, true, null);
        return response()->json($cats);
    }

    public function getArticleByDoctorId(Request $request)
    {
        $doctorId = $request->route('doctorId');
        $arts = $this->articleModel->getArticleByDoctorId($doctorId);
        $arts = $this->resp->getResponse($arts, Response::HTTP_NOT_FOUND, null, true, null);
        return response()->json($arts);
    }


}
