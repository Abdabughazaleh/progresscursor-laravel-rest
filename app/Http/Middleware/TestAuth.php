<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rs = DB::table('users')
            ->where('email', '=', $request->get('email'))
            ->where('password', '=', md5($request->get('password')))
            ->first();

        if ($rs !=null)
            return $next($request);

        return redirect('error');
    }
}
