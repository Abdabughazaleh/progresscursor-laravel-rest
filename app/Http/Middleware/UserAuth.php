<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\UserModel as userModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAuth
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        try {
            $headers = apache_request_headers();
            $request->headers->set('Authorization', $headers['Authorization']);
            $token = JWTAuth::getToken();
            JWTAuth::getPayload($token)->toArray();
            return $next($request);
        } catch (TokenInvalidException $e) {
            return response()->json(['status' => 'Token is Invalid']);
        }catch (TokenExpiredException $e){
            return response()->json(['status' => 'Token is Expired']);
        }catch (\Exception $e){
            return response()->json(['status' => 'Authorization Token not found']);
        }

    }
}
