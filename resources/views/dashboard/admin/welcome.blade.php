@extends('dashboard.layout.structure')
@section('title')
    Welcome page
@endsection

@section('head')

@endsection
@section('page-name')
    Welcome  Page
@endsection
@section('root-page')
    Home
@endsection
@section('parent-page')
    Welcome
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>tEST</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" id="resultx">
                    username: <input type="text" id="user">
                    <button class="btn btn-primary" onclick="reg()">Register</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')


    <script>
        function reg() {

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: {
                    "_token": "{{csrf_token()}}",
                    "user": $("#user").val()
                },
                url: "{{URL('/test/register')}}",
                success(resp) {
                    console.log(resp);
                    if (resp.status == "success")
                        toastr.success('User Is Abd', 'Success');
                    else
                        toastr.warning('Not abd', 'Warning');
                }
            });
        }

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'http://localhost:8083/tab-api/api/articles/getArticles',
            success(resp) {
                for (let i = 0; i < resp.list.length; i++) {
                    //  console.log(resp.list[i].artDescription);
                    // $("#resultx").append('   <div dir="rtl" class="panel">\n' +
                    //     '                        <div class="panel-heading">\n' +
                    //     '                            <h1>'+resp.list[i].artTitle+'</h1>\n' +
                    //     '                        </div>\n' +
                    //     '                        <div class="panel-body">\n' +
                    //     '                            '+resp.list[i].artDescription+'\n' +
                    //     '                        </div>\n' +
                    //     '                    </div> <hr>');
                }
            }
        });
    </script>
@endsection
