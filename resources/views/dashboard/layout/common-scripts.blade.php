
<!-- Mainly scripts -->
<script src="{{URL('public/dashboard/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{URL('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{URL('public/dashboard/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{URL('public/dashboard/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Peity -->
<script src="{{URL('public/dashboard/js/plugins/peity/jquery.peity.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{URL('public/dashboard/js/inspinia.js')}}"></script>
<script src="{{URL('public/dashboard/js/plugins/pace/pace.min.js')}}"></script>

<!-- jquery UI -->
<script src="{{URL('public/dashboard/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- Touch Punch - Touch Event Support for jQuery UI -->
<script src="{{URL('public/dashboard/js/plugins/touchpunch/jquery.ui.touch-punch.min.js')}}"></script>

<!-- iCheck -->
<script
    src="{{URL('public/dashboard/js/plugins/iCheck/icheck.min.js')}}"></script>

<!-- Peity d data  -->
<script src="{{URL('public/dashboard/js/demo/peity-demo.js')}}"></script>


<!-- Toastr script -->
<script src="{{URL('public/dashboard/js/plugins/toastr/toastr.min.js')}}"></script>
