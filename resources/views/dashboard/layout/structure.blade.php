<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="{{URL('public/dashboard/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL('public/dashboard/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{URL('public/dashboard/css/animate.css')}}" rel="stylesheet">
    <link href="{{URL('public/dashboard/css/style.css')}}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{URL('public/dashboard/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    @yield('head')

</head>

<body>

<div id="wrapper">

    <!-- SideBar -->
@include('dashboard.layout.sidebar')
<!--./ SideBar -->

    <div id="page-wrapper" class="gray-bg">
        <!-- TopSideBar -->
    @include('dashboard.layout.top-side-bar')
    <!--./ TopSideBar -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>@yield('page-name')</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">@yield('root-page')</a>
                    </li>
                    <li>
                        <a>@yield('parent-page')</a>
                    </li>
                    <li class="active">
                        <strong>@yield('current-page')</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
                @yield('small-page-detail')
            </div>
        </div>

        <div class="wrapper wrapper-content  animated fadeInRight">
            @yield('content')
        </div>
        @include('dashboard.layout.footer')

    </div>
</div>

@include('dashboard.layout.common-scripts')
@yield('custom-scripts')

</body>

</html>
