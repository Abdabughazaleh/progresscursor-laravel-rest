<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'UserController@login');
/*--------------  Articles Route ------------------*/
Route::get('/articles/getArticles', 'Articles\ArticleController@getArticles');
Route::get('/articles/getArticleByDoctorId/{doctorId}', 'Articles\ArticleController@getArticleByDoctorId');
Route::get('/articles/getArticlesByOrderByCats', 'Articles\ArticleController@getArticlesByOrderByCats');
Route::get('/articles/getArticleById/{id}', 'Articles\ArticleController@getArticleById')
    ->where(['id' => '[0-9]+']);
/*--------------End Articles Route ------------------*/

/*--------------  Doctor Route ------------------*/
Route::get('/doctors/getDoctorById/{id}', 'Doctor\DoctorController@getDoctorById')
    ->where(['id' => '[0-9]+']);
Route::get('/doctors/doctorSearch/{id}', 'Doctor\DoctorController@doctorSearch')
    ->where(['id' => '[0-9]+']);
Route::get('/doctors/getDoctorWorkingDays/{id}', 'Doctor\DoctorController@getDoctorWorkingDays')
    ->where(['id' => '[0-9]+']);

/*--------------End Doctor Route ------------------*/

/*--------------  Doctor Route ------------------*/
Route::get('/specialties/getAll', 'Specialties\SpecialtiesController@getAll');
Route::get('/specialties/getSpecById/{id}', 'Specialties\SpecialtiesController@getSpecById')
    ->where(['id' => '[0-9]+']);
/*--------------End Doctor Route ------------------*/
/*--------------  Doctor Route ------------------*/
Route::get('/countries/getAllCountries', 'Countries\CountriesController@getAllCountries');
Route::get('/countries/getALLCities/{id}', 'Countries\CountriesController@getAllCitiesByCountryId')
    ->where(['id' => '[0-9]+']);
Route::get('/countries/getSubCitiesByCityId/{id}', 'Countries\CountriesController@getSubCitiesByCityId')
    ->where(['id' => '[0-9]+']);

/*--------------End Doctor Route ------------------*/

Route::group(['middleware' => ['user.auth:userAuth']], function () {
    Route::get('/get', 'UserController@generateKey');
});



